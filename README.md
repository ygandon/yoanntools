For bash tools, add the following in .bashrc :  

```  

function _yoBashSourceHttpContent(){
	local url=$1;
	local fileName=$(echo -e $url | sed 's/.*\///');
	
	local tempFolder='.';
	if [ ! -z "$TMP" ]
	then
		tempFolder=$TMP;
	fi
	
	local localFile=$tempFolder/$fileName;
	
	echo ;
	echo Downloading $localFile from $url ...;
	curl -s -o $localFile $url;
	
	echo Downloaded. Sourcing $localFile ...;
	source $localFile;
	
	echo Sourced. Removing $localFile ...;
	rm $localFile;
	echo Removed. Successfully sourced remote $url.;
}

YOANN_TOOLS_BASH_BITBUCKET_BASE_URL='https://bitbucket.org/ygandon/yoanntools/raw/master/bash/'
_yoBashSourceHttpContent $YOANN_TOOLS_BASH_BITBUCKET_BASE_URL.bashrc_YoannCommon.sh
_yoBashSourceHttpContent $YOANN_TOOLS_BASH_BITBUCKET_BASE_URL.bashrc_YoannPS1.sh
_yoBashSourceHttpContent $YOANN_TOOLS_BASH_BITBUCKET_BASE_URL.bashrc_YoannGit.sh

```  
