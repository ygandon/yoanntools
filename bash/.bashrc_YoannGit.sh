SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

alias gitk='gitk --all --date-order & '
alias yogitFetchAll='git fetch --all -a -v'
alias yogitRebase='git rebase --autostash'
alias yogitAmend='git commit --amend --no-edit'
alias yogitYoannPointerRefresh='git branch -f YOANN_POINTER HEAD'
alias yogitLog='git log --graph --format=format:'\''%C(bold blue)%H%C(reset) - %C(dim white)%aN (%ae)%C(reset)%C(bold yellow)%d%C(reset)%n%C(bold green)%ad (%ar)%C(reset)%n%C(white)%B%C(reset)%n'\'''
alias yogitLogBodies='git log --format="----------------------------------%n%B"'
alias yogitUsers='git log --format="%an%n%ae" | sort -u'
alias yogitCloneRootAbsolutePath='$(git rev-parse --show-toplevel)'
alias cdGitCloneRoot='gitCloneRoot="$(yogitCloneRootAbsolutePath)" && test -n "$gitCloneRoot" && cd "$gitCloneRoot"'


#_yoBash_save_function _yoBashWorkingDirectoryHint _yoBashWorkingDirectoryHint_prevToGit
_yoBashWorkingDirectoryHint(){
	#echo -e "`_yoBashWorkingDirectoryHint_prevToGit``__git_ps1`"
	__git_ps1
}


_yoBashWorkingDirectorySummaryForTitle(){
	gitRepoTopLevel_windows="$(git rev-parse --show-toplevel 2>/dev/null)"
	
	if [  $? -eq 0 ]; then
		if [ -z "$gitRepoTopLevel_windows" ]; then # If output is empty, we are in .git directory
			gitRepoTopLevel="$(echo $PWD | sed 's/\/\.git.*//')"
		else
			gitRepoTopLevel="$(cygpath.exe $gitRepoTopLevel_windows)"
		fi
		
		
		#echo "gitRepoTopLevel : "$gitRepoTopLevel
		repoName="$(echo $gitRepoTopLevel | sed 's/.*\///')"
		#echo "repoName : "$repoName
		length=${#repoName}
		toRemoveFromPWD=${gitRepoTopLevel::-length}
		#echo "toRemoveFromPWD : "$toRemoveFromPWD
		
		length=${#toRemoveFromPWD}
		length=$((length+1)) # adding last slash
		echo $PWD | cut -c $length-
	else
		echo $PWD
	fi
}
