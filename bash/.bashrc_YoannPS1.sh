SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo
echo "PS1 was :"
echo "$PS1"

PS1=""
PS1=$PS1"\e[0;30m"				#change color to black
PS1=$PS1"\e[1;30m"				#change color to dark grey (LightBlack)
PS1=$PS1"\e[0;31m"				#change color to dark red (Red)
PS1=$PS1"\e[1;31m"				#change color to red (LightRed)
PS1=$PS1"\e[0;32m"				#change color to dark green (Green)
PS1=$PS1"\e[1;32m"				#change color to green (LightGreen)
PS1=$PS1"\e[0;33m"				#change color to dark yellow (Brown)
PS1=$PS1"\e[1;33m"				#change color to yellow (LightBrown)
PS1=$PS1"\e[0;34m"				#change color to dark blue (Blue)
PS1=$PS1"\e[1;34m"				#change color to blue (LightBlue)
PS1=$PS1"\e[0;35m"				#change color to purple
PS1=$PS1"\e[1;35m"				#change color to pink (LightPurple)
PS1=$PS1"\e[0;36m"				#change color to dark cyan (Cyan)
PS1=$PS1"\e[1;36m"				#change color to cyan (LightCyan)

PS1=$PS1"\e[0;37m"				#change color to grey (White)
PS1=$PS1"\e[1;37m"				#change color to white (LightWhite)
PS1=$PS1"$_YOANN_BASH_RESET_COLOR"			#reset colors
_YOANN_BASH_RESET_COLOR="\e[m"

## Public functions - shell window title
function yoBashTitlePrefix() {
	_YOANN_BASH_TITLE_PREFIX="$@"
	
	if [ -z "'$_YOANN_BASH_TITLE_PREFIX'" ]
	then
		unset _YOANN_BASH_TITLE_PREFIX
	fi
}

function yoBashTitleSuffix() {
	_YOANN_BASH_TITLE_SUFFIX="$@"
	
	if [ -z "'$_YOANN_BASH_TITLE_SUFFIX'" ]
	then
		unset _YOANN_BASH_TITLE_SUFFIX
	fi
}

function yoBashTitleFix() {
	_YOANN_BASH_TITLE_FIX="$@"
	
	if [ -z "'$_YOANN_BASH_TITLE_FIX'" ]
	then
		unset _YOANN_BASH_TITLE_FIX
	fi
}



# Internal functions : PS1 prompt dynamic processing
function _yoBashBuildTitle() {
	local previousReturnValue=$?
	#>&2 echo "EXECUTED : $? - $previousReturnValue"
	if [ ! -z "$_YOANN_BASH_TITLE_FIX" ]
	then
		echo -e $_YOANN_BASH_TITLE_FIX
		return
	fi

	local titleResult=""
	
	if [ ! -z "$_YOANN_BASH_TITLE_PREFIX" ]
	then
		titleResult=$_YOANN_BASH_TITLE_PREFIX" "$titleResult
	fi
	
	titleResult=$titleResult`_yoBashWorkingDirectorySummaryForTitle`
	
	
	if [ ! -z "$_YOANN_BASH_TITLE_SUFFIX" ]
	then
		titleResult=$titleResult" "$_YOANN_BASH_TITLE_SUFFIX
	fi
	
	echo -e $titleResult
	#>&2 echo "EXECUTED 2 : $? - $previousReturnValue"
	return $previousReturnValue
}


_yoBashWorkingDirectorySummaryForTitle(){
	echo -e $PWD
}

_yoBashWorkingDirectoryHint(){
	echo -e ""
}


PS1=""

PS1=$PS1"\e]0;"'`_yoBashBuildTitle`'"\a"

PS1=$PS1"\n"
PS1=$PS1"\$? -> "
PS1=$PS1"\`if [ \$? = 0 ]; then echo \[\e[1\;32m\]^_^\[\e[0m\]; else echo \[\e[1\;31m\]O_O\[\e[0m\]; fi\`"
PS1=$PS1"\n"

PS1=$PS1"\e[1;32m"			#change color to light green
PS1=$PS1"\u"
PS1=$PS1"\e[1;33m"			#change color to yellow
PS1=$PS1"@"
PS1=$PS1"\e[1;32m"			#change color to light green
PS1=$PS1"\h"
PS1=$PS1"$_YOANN_BASH_RESET_COLOR"			#reset colors

PS1=$PS1"\e[1;33m"			#change color to yellow
PS1=$PS1" - "



PS1=$PS1"\e[1;32m"			#change color to light green
PS1=$PS1"\D{%Y-%m(%B)-%d}"			#Date
PS1=$PS1"\e[1;33m"			#change color to yellow
PS1=$PS1"\D{ %A }"			#Week day
PS1=$PS1"\e[1;32m"			#change color to light green
PS1=$PS1"\D{%H:%M:%S}"			#Time
PS1=$PS1"$_YOANN_BASH_RESET_COLOR"			#reset colors



PS1=$PS1"\n"
PS1=$PS1"\\e[1;33m"				#change color to yellow
PS1=$PS1"\$PWD"

PS1=$PS1"\e[0;36m"				#change color to dark cyan (Cyan)
PS1=$PS1'`_yoBashWorkingDirectoryHint`'

PS1=$PS1"$_YOANN_BASH_RESET_COLOR"			#reset colors

PS1=$PS1"\n"
PS1=$PS1"\! \\$ "

echo
echo "PS1 is :"
echo "$PS1"
echo
