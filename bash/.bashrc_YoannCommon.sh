SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"





#alias ls='ls -F --color=auto --show-control-chars'
alias l='ls -lAh --group-directories-first -F --color=auto --show-control-chars'
alias ll='l'
alias lll='l'

alias h='history'
alias env='env | sort '

alias xargs='xargs '
alias yoGrep='grep --color -n '

# expected resolved alias : sed 's/^/'"'"'/' | sed 's/$/'"'"'/'
alias yoQuoteLine='sed '"'"'s/^/'"'"'"'"'"'"'"'"'/'"'"' | sed '"'"'s/$/'"'"'"'"'"'"'"'"'/'"'"''

alias rg='find . -type f | yoQuoteLine | xargs yoGrep '
alias rgj='find . -type f -iname "*.java" | yoQuoteLine | xargs yoGrep '
alias rgpom='find . -type f -iname "pom.xml" | yoQuoteLine | xargs yoGrep '
alias rghtml='find . -type f -iname "*.html" | yoQuoteLine | xargs yoGrep '
alias rgts='find . -type f -iname "*.ts" | yoQuoteLine | xargs yoGrep '
alias rgcss='find . -type f -iname "*.css" -o -iname "*.scss" | yoQuoteLine | xargs yoGrep '

alias mcp='mvn clean package'
alias mcv='mvn clean verify'
alias mci='mvn clean install'

alias cdh='cd $USERPROFILE'
alias cdhd='cd $USERPROFILE/Desktop'

alias yoExploreHere='explorer . &'

alias yoBashResetShell='exec bash --login -l -i'


_yoBash_save_function() {
	local ORIG_FUNC=$(declare -f $1)
	local NEWNAME_FUNC="$2${ORIG_FUNC#$1}"
	eval "$NEWNAME_FUNC"
	echo "Function "$ORIG_FUNC" copied in "$NEWNAME_FUNC
}

